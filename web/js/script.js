$(document).ready(function() {
	$(".nd-slider").slick({
		arrows: false,
		dots: true,
		fade: true,
		autoplay: true,
		autoplaySpeed: 3000,
	});
	// $(".nd-slides").slick({
	// 	arrows: false,
	// 	dots: false,
	// 	centerMode: true,
	// 	centerPadding: '60px',
	// 	slidesToShow: 1
	// });

	if ($(window).width()>1320) {
		$(".nd-slides").slick({
			arrows: false,
			dots: false,
			centerMode: true,
			centerPadding: '50px',
			slidesToShow: 2,
			swipeToSlide: true,
			swipe: true,
			focusOnSelect: true
		});
	}

	$(".halfs .left").mouseenter(function(event) {
		$("#mi1").addClass('moved');
	});
	$(".halfs .left").mouseleave(function(event) {
		$("#mi1").removeClass('moved');
	});
	$(".halfs .right").mouseenter(function(event) {
		$("#mi2").addClass('moved');
	});
	$(".halfs .right").mouseleave(function(event) {
		$("#mi2").removeClass('moved');
	});

	$('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	      	$("body, html").removeClass('ovh');
	      	$(".overlay-menu-wrap, .menu-trigger").removeClass('is-active');
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	});
	$(".nd-year-switch .nd-year").click(function(event) {
		$(this).addClass('is-active').siblings().removeClass('is-active');
		if ($(this).is(':last-child')) {
			$(this).parent().addClass('second');
			animateCounters("second");
		}
		else {
			$(this).parent().removeClass('second');	
			animateCounters("first");
		}
	});

	$("body").on('click touch', '.to-popup', function(event) {
		event.preventDefault();
		var pop = $(this).data("popup");
		if(!pop) return;
		openPopup(pop);
	});

	$(".nd-popup").click(function(e){
	  e.stopPropagation();
	});
	$(".js-closepop, .nd-popup-wrap").click(function(event) {
		event.preventDefault();
		closePopups();
	});

	$(".nd-popup-wrap").click(function(event) {
		event.preventDefault();
		closePopups();
	});

	$("#js-change").click(function(event) {
		event.preventDefault();
		$(this).siblings('input').val("").attr('placeholder', 'Введiть суму').focus();
		$('.nd-type-select input[type=radio]:checked').prop('checked', false);
	});

	$(".nd-showmore-wrap a").click(function(event) {
		event.preventDefault();
		$(this).parents(".nd-showmore-wrap").remove();
	});

});

$(window).scroll(function() {
    if ($('.map').isOnScreen() == true) {
       $(".map").addClass('visible');
    }
    if ( $(".nd-stats:not(.animated)").length ) {
		animateCounters("first");
		$(".nd-stats").addClass('animated');
    }
});

$.fn.isOnScreen = function(){
    
    var win = $(window);

    var offset = win.height()/2 - 100;
    
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height() - offset;
    
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
    
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    
};

var firstanim = true;

function animateCounters(mode) {
	var start = 0;
	
	$('.nd-counter').each(function () {
		if ( (mode == "first") && (firstanim == false) ){
			start = $(this).data("second")
		}
		if (mode !== "first") {
			start = $(this).data("first")
		}
	    $(this).prop('Counter',start).animate({
	        Counter: $(this).data(mode)
	    }, {
	        duration: 2000,
	        easing: 'swing',
	        step: function (now) {
	            $(this).text(numberWithSpaces(Math.ceil(now)));
	        }
	    });
	});
	firstanim = false;	
}


function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

$(document).keydown(function(e) {
    // ESCAPE key pressed
    if (e.keyCode == 27) {
        closePopups();
    }
});

function openPopup(href) {
	if ($(".nd-popup:visible").length) {
		$(".nd-popup").hide();
	}
	$(".nd-popup-wrap")
		.addClass('active')
	    .hide()
	    .fadeIn();
	$(".nd-popup#" + href).show();
	$("body").addClass('ovh');
}


function closePopups() {
	$(".nd-popup-wrap").fadeOut(400, function(){
		$(".nd-popup").hide();
	});
	$("body").removeClass('ovh');
}
